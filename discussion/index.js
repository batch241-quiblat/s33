// console.log("Hello world!");
// conosle.log("Hi B241!");
// console.log("Hi B241!");

// [SECTION] JavaScript Synchronous vs Asynchronous
// Asynchronous - means that we can proceed to execute other statements, while time consuming code is running in the background.

// Fetch() method retuns a promise that resolves to a response object
// promise - is an object that represents the eventual completion (or failure) of an asynchronouse function and its resulting value
console.log(fetch('https://jsonplaceholder.typicode.com/posts'));

// Fetch() method retuns a promise that resolves to a "response object"
fetch('https://jsonplaceholder.typicode.com/posts')
// then() capstures the "response object" and returns another promise which will eventually be resolved or rejected
.then(response => console.log(response.status));


fetch('https://jsonplaceholder.typicode.com/posts')
// "json()" - from the "response object" to convert the data retrieved into JSON format to be used in our application
.then(response => response.json())
// Print the converted JSON value from the "fetch" request
.then((json) => console.log(json));
// "promise chain" - using multiple .then()


// async and await
// The "async" and "await" keywords is another approach that can be used to achieve asynchronous code
// Used in functions to indicate which portions of code should be waited for
async function fetchData(){
	// waits for the "fetch" method to complete then stores the value in the "result" variable
	let result = await fetch('https://jsonplaceholder.typicode.com/posts');
	// Result returned by fetch is a returns a promise
	console.log(result);
	// The returned "Response" is an object
	console.log(typeof result);
	// We cannot access the content of the "Response" by directly accessing it's body property
	console.log(result.body);

	// Converts the data from the "Response" object as JSON
	let json = await result.json();
	// Print out the content of the "Response" object
	console.log(json);
};
fetchData();


// [SECTION] Creating a post
fetch('https://jsonplaceholder.typicode.com/posts', {
	// Sets the method of the "request object"
	method: 'POST',
	// Sets the header data of the "request object" to be sent to the backend
	headers: {
		'Content-Type': 'application/json'
	},
	// JSON.stringify - converts the object data into a stringified JSON
	body: JSON.stringify({
		"userId": 1,
    	"title": "Create Post",
    	"body": "Create Post File"
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// Mini-activity
// [SECTION] Updating a post using PUT method
fetch('https://jsonplaceholder.typicode.com/posts/1', {
	// Sets the method of the "request object"
	method: 'PUT',
	// Sets the header data of the "request object" to be sent to the backend
	headers: {
		'Content-Type': 'application/json'
	},
	// JSON.stringify - converts the object data into a stringified JSON
	body: JSON.stringify({
		"userId": 1,
    	"title": "Update Post",
    	"body": "Update Post File"
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// [SECTION] Deleting a post
fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'DELETE'
}).then(() => console.log("Successfully deleted"));