// Fetch All Data
fetch('https://jsonplaceholder.typicode.com/todos')
.then(response => response.json())
.then((json) => json.map((todo) => todo.title))
.then((titles) => console.log(titles));


// GET
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'GET'
}).then((response) => response.json()).then((json) => {
	console.log(json)
	console.log(`The item ${json.title} on the list has a status of ${json.completed}`)
	});

// POST
fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST',
	headers: {'Content-Type':'application/json'},
	body: JSON.stringify({
		"completed": false,
	    "title": "Created To Do List Item",
	    "userId": 1
	})
}).then((response) => response.json()).then((json) => console.log(json));

// Update PUT
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers: {'Content-Type':'application/json'},
	body: JSON.stringify({
		"dateCompleted": "pending",
		"description":"To update the my to do list with a different data structure",
		"status": "Pending",
	    "title": "Updated To Do List Item",
	    "userId": 1
	})
}).then((response) => response.json()).then((json) => console.log(json));

// Update PATCH
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers: {'Content-Type':'application/json'},
	body: JSON.stringify({
		"completed": false,
		"dateCompleted": "07/09/21",
		"status": "Complete",
	    "title": "delectus aut autem",
	    "userId": 1
	})
}).then((response) => response.json()).then((json) => console.log(json));

// DELETE
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'DELETE'
});